#!/usr/bin/env python
"""
Check the %mor tier of a set of .cha files, look for inconsistent tagging in context.
Report n-grams where the lexical items are the same but the tags differ.
"""

import sys
import codecs
import os
from argparse import ArgumentParser
from collections import defaultdict


def readCha(filename, data, grams, simple):
	"""
	Read cha file and process %mor tiers
	%mor entries are indicated with column-1 heading, but optionally include further lines
	"""
	utterance = ""
	for line in open(filename, "r"):
		line = line.strip("\n\r")
		if line.startswith("%mor"):
			utterance = line.split("\t")[1]
		elif line.startswith("\t") and utterance:
			utterance += " " + line[1:]
		elif utterance:
			data = process(utterance, data, grams, simple)
			utterance = ""
	return data


def process(utterance, data, grams, simple):
	"""Take string from %mor tier and add to n-gram sets."""
	utterance = "START|START " + utterance + " END|END"
	sequence = utterance.split(" ")
	tagSequence = []
	wordSequence = []

	for item in sequence:
		if "|" in item:
			try:
				pos, word = item.split("|")
			except ValueError:
				print >>sys.stderr, "Too many pipes! %s" % item
				continue
			word = word.split("&")[0]
			if word.endswith("im") and len(word) > 3:
				word = word[:-2]
			if simple:
				tagSequence.append(pos)
			else:
				tagSequence.append(item)
			wordSequence.append(word)


	if len(tagSequence) >= grams:
		for i in range(0,( len(tagSequence) - grams)):
			tagNgram = tagSequence[i]
			wordNgram = wordSequence[i]
			for x in range(1,grams):
				tagNgram += " " + tagSequence[i + x]
				wordNgram += " " + wordSequence[i + x]
			data[wordNgram][tagNgram] += 1

	return data


def main():
	parser = ArgumentParser(description=__doc__)
	parser.add_argument("input", help="CHA file to check")
	parser.add_argument("-n", "--nGramSize", type=int, default=4, help="size of n-grams, default 4")
	parser.add_argument("-s", "--simple", action="store_true", help="Print output with just POS tags instead of full markup")
	opts = parser.parse_args()
	data = defaultdict(lambda: defaultdict(int))
	for root, dirs, files in os.walk(opts.input, topdown=False):
		for name in files:
			filename = os.path.join(root,name)
			if name.endswith("cha"):
				data = readCha(filename, data, opts.nGramSize, opts.simple)
	
	for wordNgram in data.keys():
		tagNgramDict = data[wordNgram]
		if len(tagNgramDict) > 1:
			if len(tagNgramDict) < 4:
				wordList = wordNgram.split(" ")
				tagList = tagNgramDict.keys()
				firstList = tagList[0].split(" ")
				secondList = tagList[1].split(" ")
				if len(tagNgramDict) == 3:
					thirdList = tagList[2].split(" ")
				for i, element in enumerate(firstList):
					wordDiff = wordList[i]
					if len(tagNgramDict) == 3:
						if not element == secondList[i] == thirdList[i]:
							tagAndFreqDiff = [element.split("|")[0], str(tagNgramDict[tagList[0]]), secondList[i].split("|")[0], str(tagNgramDict[tagList[1]]), thirdList[i].split("|")[0], str(tagNgramDict[tagList[2]])]
							print "\t".join([wordNgram, wordDiff] + tagAndFreqDiff + tagList)
					elif len(tagNgramDict) == 2:
						if element != secondList[i]:
							tagAndFreqDiff = [element.split("|")[0], str(tagNgramDict[tagList[0]]), secondList[i].split("|")[0], str(tagNgramDict[tagList[1]]), "", ""]
							print "\t".join([wordNgram, wordDiff] + tagAndFreqDiff + tagList)
			else: #too long, just print full entries
				print wordNgram + "\t\t\t\t" + "\t".join(tagNgramDict.keys())
		if "_tu" in wordNgram:
			altKey = wordNgram.replace("_tu", "_ku")
			if altKey in data.keys():
				for tags in tagNgramDict:
					if tags in data[altKey]:
						print wordNgram.replace("_tu_", "_tu/_ku") + "\t" + tags

if __name__=="__main__":
	main()
