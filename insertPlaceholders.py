#!/usr/bin/env python
"""
Only run this script after you have run both countTokens.py and checkMorcodeMatches.py
with no output.
"""

import sys
import codecs
import textwrap
from argparse import ArgumentParser,RawTextHelpFormatter
import os
import re

sys.stdout=codecs.getwriter('utf8')(sys.stdout)

def isMissing(token,code,uncertain):
	if uncertain:
		return True
	elif token.startswith('&') or token == 'xxx' or token == '+"':
		return True
	else:
		return False

def insertTokens(tx,mor):
	uncertain = False
	txindex = 0
	newmor = []
	
	for code in mor:
		if txindex < len(tx):
			if '[' in tx[txindex]:
				uncertain = True
			try:
				while isMissing(tx[txindex],code,uncertain):
					if ']' in tx[txindex]:
						bracketed = '['+re.sub('[\[\]]','',tx[txindex])+']'
						newmor.append(bracketed)
						uncertain = False
					elif tx[txindex].startswith('['):
						bracketed = '['+re.sub('[\[\]]','',tx[txindex])+']'
						newmor.append(bracketed)
					elif uncertain:
						bracketed = '['+re.sub('[\[\]]','',tx[txindex])+']'
						newmor.append(bracketed)
					else:
						newmor.append(tx[txindex])
					txindex += 1
			except IndexError:
				print >> "This line didn't work: "+sys.stderr,' '.join(tx)
		newmor.append(code)
		txindex += 1
	return newmor

def main():
	parser = ArgumentParser(description = __doc__,formatter_class=RawTextHelpFormatter)
	parser.add_argument('input' ,help = 'giant text file containing all .cha files')
	opts = parser.parse_args()

	txtier = False
	mortier = False
	tx = []
	cleantx = []
	mor = []
	newmor = []
	for origline in codecs.open(opts.input, 'r', 'utf8'):
		origline = origline.strip('\n')
		endofutt = False
		filepath = re.search(r'^(.*\.cha:)',origline).group(1)
		# cleaning up, getting rid of file path
		line = origline.strip('\n')
		line = line.strip(' ')
		line = re.sub(r'^[^:]*:','',line)
		line = re.sub('  ',' ',line)
		# Which tier are we on?
		if line.startswith('%mor'):
			mortier = True
			txtier = False
		elif not line.startswith('\t'):
			mortier = False
			if tx and mor:
				# do stuff here
				newmor = insertTokens(cleantx,mor)
				wrapped = textwrap.wrap(' '.join(newmor),80,break_long_words=False,break_on_hyphens=False)
				print filepath+'%mor:\t'+wrapped[0]
				for morline in wrapped[1:]:
					print filepath+'\t'+morline
				tx = []
				mor = []
				cleantx = []
				newmor = []
			if line.startswith('*'):
				tx = []
				cleantx = []
				mor = []
				txtier = True
				mortier = False
			else:
				txtier = False
		if not mortier:
			print origline

		# Transcription tier
		if txtier:
			uncertain = False
			line = re.sub('([,.!?])',r' \1',line)
			line = re.sub('  ',' ',line)
			if '\t' in line:
				secondcol = line.split('\t')[1]
				tokens = secondcol.split(' ')
				for token in tokens:
					if not u'\u0015' in token:
						tx.append(token)
						cleantx.append(token.replace('+[',''))

		if mortier:
			line = re.sub('  ',' ',line)
			secondcol = line.split('\t')[1]
			codes = secondcol.split(' ')
			for code in codes:
				if code:
					mor.append(code)

if __name__=="__main__":
	main()
