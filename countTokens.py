#!/usr/bin/env python
"""
Counts the number of tokens in the transcription tier to the number of tokens in the mor tier.

Input file is a large text file with all the .cha files grepped together, and file paths
at the start of each line.

Output can be copied into a spreadsheet - look up the line numbers and edit manually.
"""

import sys
import codecs
from argparse import ArgumentParser,RawTextHelpFormatter
import os
import re

sys.stdout=codecs.getwriter('utf8')(sys.stdout)

def test(txtier,mortier,line,tx,mor):
	#testing
	if txtier:
		print 'tx'+'\t'+str(len(tx))+'\t'+line
	elif mortier:
		print 'mor'+'\t'+str(len(mor))+'\t'+line
	else:
			print '\t\t'+line

def validate(tx,mor,lineref):
	if len(tx) != len(mor):
		difference = len(tx)-len(mor)
		print str(lineref)+'\t'+str(difference),
		print '\t'+'\t'.join(tx)
		print '\t\t'+'\t'.join(mor)
		print
		return 1
	else:
		return 0

def main():
	parser = ArgumentParser(description = __doc__,formatter_class=RawTextHelpFormatter)
	parser.add_argument('input',help = 'giant text file containing all .cha files')
	parser.add_argument('-p', '--placeholder', action='store_true', help='Use this option if the input file already has placeholders.')
	opts = parser.parse_args()

	linenumber = 0
	count = 0
	lineref = 0
	txtier = False
	mortier = False
	tx = []
	mor = []
	for line in codecs.open(opts.input, 'r', 'utf8'):
		endofutt = False
		linenumber += 1
		# cleaning up, getting rid of file path
		line = line.strip('\n')
		line = line.strip(' ')
		line = re.sub(r'^[^:]*:','',line)
		line = re.sub('  ',' ',line)
		# Which tier are we on?
		if line.startswith('%mor'):
			mortier = True
			txtier = False
		elif not line.startswith('\t'):
			mortier = False
			if tx and mor:
				count += validate(tx,mor,lineref)
				tx = []
				mor = []
				lineref = 0
			if line.startswith('*'):
				tx = []
				mor = []
				lineref = linenumber
				txtier = True
				mortier = False
			else:
				txtier = False

		# Transcription tier
		if txtier:
			uncertain = False
			line = re.sub('([,.!?])',r' \1',line)
			line = re.sub('  ',' ',line)
			if '\t' in line:
				secondcol = line.split('\t')[1]
				tokens = secondcol.split(' ')
				for token in tokens:
					if '[' in token:
						if not '+[' in token:
							uncertain = True
					if token:
						if not u'\u0015' in token:
							if not opts.placeholder:	
								if not uncertain:
									if not token.startswith('&'):
										if not 'xxx' in token:
											if token != '+"':
												tx.append(token)
							else:
								tx.append(token)
					if ']' in token:
						uncertain = False

		if mortier:
			line = re.sub('  ',' ',line)
			secondcol = line.split('\t')[1]
			codes = secondcol.split(' ')
			for code in codes:
				if code:
					mor.append(code)

		#test(txtier,mortier,line,tx,mor)
	print >> sys.stderr,'%s utterances found with non-matching transcription/mor tiers'%(count)

if __name__=="__main__":
	main()
