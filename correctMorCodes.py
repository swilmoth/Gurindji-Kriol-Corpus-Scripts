#!/usr/bin/env python
"""
This script corrects mor-codes across all CHAT files\r
in a given directory.\r
\r
The inputs are:\r
-c checked codes file\r
	This is a two-column (tab-delimited) file with incorrect\r
	in the first column and corrections in the second.\r
-d directory with .cha files
	The script will look in all subdirectories of this directory,\r
	correcting any file with a .cha file extension.\r

Any corrections are made in a new file in the same location,\r
with the file extension .cha.correctmor
"""

import sys
import codecs
from argparse import ArgumentParser,RawTextHelpFormatter
import os
import re

sys.stdout=codecs.getwriter('utf8')(sys.stdout)

def main():
	parser = ArgumentParser(description = __doc__,formatter_class=RawTextHelpFormatter)
	parser.add_argument('-c', '--checkedcodes') # Checked codes
	parser.add_argument('-d', '--indir', nargs = '?') # Directory with .cha files
	parser.add_argument('-i', '--inputfile', nargs = '?') # text file with all .cha files and filepaths
	opts = parser.parse_args()

	# read checked codes file, make dictionary
	checkedcodes = {}
	for line in codecs.open(opts.checkedcodes, 'r', 'utf8'):
		line = line.strip('\n')
		badMor = line.split('\t')[0]
		goodMor = line.split('\t')[1]
		checkedcodes[badMor] = goodMor
		checkedcodes[badMor+'\\S'] = goodMor+'\\S'
		checkedcodes[badMor+'\\P'] = goodMor+'\\P'

	# go through directory, correct transcriptions
	if opts.indir:
		for root, dirs, files in os.walk(opts.indir, topdown=False):
			for name in files:
				morline = False
				correctedFile = False
				fullpath = os.path.join(root,name)
				if name.endswith(".cha"):
					outfile = []
					for line in codecs.open(fullpath, 'r', 'utf8'):
						line = line.strip('\n')
						mortier = []
						if line.startswith('%mor'):
							morline = True
						elif not line.startswith('%mor') and not line.startswith('\t'):
							morline = False
						if morline:
							if '\t' in line:
								morcodes = line.split('\t')[1]
								morcodelist = morcodes.split(' ')
								correctmorline = []
								# corrections
								for index,code in enumerate(morcodelist):
									if code in checkedcodes:
										correctmorline.append(checkedcodes[code])
										correctedFile = True
									else:
										correctmorline.append(code)
										
								correctline = line.split('\t')[0] + '\t' + ' '.join(correctmorline)
								outfile.append(correctline)
						else:
							outfile.append(line)
					# Printing!
					if correctedFile:
						newFile = fullpath.replace(".cha",".cha.correctmor")
						sys.stdout = open(newFile,'w')
						for line in outfile:
							print line.encode('utf8')
	elif opts.inputfile:
		for origline in codecs.open(opts.inputfile, 'r', 'utf8'):
			mortier = []
			origline = origline.strip('\n')
			filepath = re.search(r'^(.*\.cha:)',origline).group(1)
			line = re.search(r'^.*\.cha:(.*)',origline).group(1)
			if line.startswith('%mor'):
				morline = True
			elif not line.startswith('%mor') and not line.startswith('\t'):
				morline = False
			if morline:
				if '\t' in line:
					morcodes = line.split('\t')[1]
					morcodelist = morcodes.split(' ')
					correctmorline = []
					# corrections
					for index,code in enumerate(morcodelist):
						if code in checkedcodes:
							correctmorline.append(checkedcodes[code])
							correctedFile = True
						else:
							correctmorline.append(code)
					correctline = line.split('\t')[0] + '\t' + ' '.join(correctmorline)
					print filepath + correctline
			else:
				print origline
	else:
		print >> sys.stderr,"Error: missing input (can be directory or file)"


						
					
if __name__=="__main__":
	main()

