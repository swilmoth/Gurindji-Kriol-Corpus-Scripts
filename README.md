# Gurindji Kriol corpus scripts
This repository contains scripts used in the development of the [Gurindji Kriol corpus](http://catalog.paradisec.org.au/collections/GK1), as described in Wilmoth & Meakins (forthcoming) 'Small language, big data: Building the Gurindji Kriol corpus to model the emergence of a mixed language'. All scripts are by Sasha Wilmoth except where otherwise noted. These scripts were originally written in Python 2.7; some minor updates may be necessary to run using Python 3 (mainly changing the syntax of print statements). I am happy to chat if you have any questions about this process or these scripts.

--Sasha Wilmoth, 2024


| Script | Description |
|--|--|
| `addFilePaths.py` | After exporting the entire from corpus into a Toolbox format, this script adds file paths, and does some other things, so that the corpus may be split into individual Toolbox files. |
| `adjustCLANtimestamps.py` | Adds or subtracts an offset to the timestamps of a CHAT file, based on a two-column file file with the session name and the offset in milliseconds. Similar to the FIXBULLETS command in CLAN, but capable of running over any number of files. |
| `av_align.py` | By Ola Olsson. Aligns audio and video, and adjusts CLAN/ELAN timestamps accordingly. It has been difficult to get this working in recent years; the most recent version from 2021 is also included here. See [here](https://coedl-knowledgebase.readthedocs.io/en/latest/posts/20170509_av_align-intro-tutorial/index.html) for more information. |
| `checkMorcodeMatches.py` | Compares tokens on the transcription to their corresponding mor-codes, compares it to the lexicon, and reports mismatches. Includes an option -p to use only the old subject coding (\S and \P, and not /S). |
| `correctCHATSpelling.py` | Corrects the transcription tier according to a two-column file with spelling errors and corrections. |
| `correctMismatches.py` | Based on the output of checkMorcodeMatches.py, this script will correct mor-codes whenever there is a particular incorrect combination of transcription and mor-code, according to a three-column mapping file. |
| `correctMorCodes.py` | Corrects the %mor tier according to a two-column file with mor-code errors and corrections. |
| `countTokens.py` | Checks that the number of tokens on the transcription tier matches the number of mor-codes. Input file is a text file with several .cha files grepped together, including file paths at the beginning of each line. |
| `finAndCopyFiles.py` | Several versions of this script were used to automate complex file management. For example, we have copies of some sessions organised into their various elicitation tasks in a separate location. This script is used to automatically update those files based on the master version of the corpus. |
| `findUnknownTokens.py` | Compares everything on the transcription tier to the lexicon, and outputs all unknown tokens. |
| `generate_headers.py` | By Jonathon Taufatofua. Generates .cha file with metadata lines from a metadata spreadsheet. Not stored in this repository, but located [here](https://github.com/CoEDL/clan-helpers/tree/master/scripts/excel2cha). 
| `insertPlaceholders.py` | Inserts some non-analysable tokens into their correct slot on the %mor tier, such as ‘xxx’ for unintelligible speech or foreign words. |
| `linkELANAudio.py` | After automatically importing everything into ELAN, the links to the media files are not included, and would be tedious to manually add using ELAN’s interface. This script modifies the underlying XML document to add media links. |
| `listTokens.py` | Simply makes a list of all tokens. The input file is a text file with several .cha files grepped together, including file paths at the beginning of each line. |
| `morChecker.py` | By Simon Hammond. Finds instances of *n* mor-codes on the mor tier, where the lemmas are identical, but the mor-codes don’t match. This is run across the whole corpus to find coding errors. |
| `morCodeLookup.py` | Interactive script for the command line that allows you to type in a word or sentence, and outputs the mor-codes according to the lexicon(s). Basically replicates the MOR command in the terminal. Option to copy to clipboard for easy pasting into transcript. |
| `reorderTiers.py` | Does some minimal re-arrangement of the CHAT files into tab-delimited columns, in order to import into OpenRefine. |
| `reportStats.py` | Quickly collects summary statistics about the corpus (or any collection of CLAN files within a particular directory and its subdirectories). Reports number of speakers, sessions, utterances, clauses, words, and morphs.
| `validateMorCodes.py` | Compares everything on the %mor tier to the lexicon, and outputs all unknown mor-codes. Includes an option -p to use only the old subject coding (\S and \P, and not /S). |

We also used a number of scripts which cannot be shared here as they belong to my former workplace and were not developed specifically for this project. The following two were particularly handy:

 - A script which lists all characters in a file (or files), and their Unicode codepoints and frequencies. This is very helpful for finding weird errors.
- I often combined the entire corpus into one big text file to work on directly. This was made by grepping everything together (as opposed to e.g. `cat`) as `grep` prints file paths at the beginning of each line when there is more than one file. Another script was then used to split this back up into individual files, either in the original locations or in a new location.