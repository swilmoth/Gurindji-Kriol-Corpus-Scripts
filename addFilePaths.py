#!/usr/bin/env python
"""
This script adds filepaths to the beginning of each line of a Toolbox file
(actually meant for multiple Toolbox files stuck together). Then you can
run splitFiles.py to make separate files.

It also does other stuff like turning the timestamps into seconds, not
milliseconds, and adds a reference number to each utterance.
"""

import sys
import codecs
import re
from argparse import ArgumentParser,RawTextHelpFormatter

sys.stdout=codecs.getwriter('utf8')(sys.stdout)
sys.stderr=codecs.getwriter('utf8')(sys.stdout)

ref = 0

def main():
	parser = ArgumentParser(description = __doc__, formatter_class=RawTextHelpFormatter)
	parser.add_argument('input', help = 'input')
	opts = parser.parse_args()
	for line in codecs.open(opts.input, 'r', 'utf8'):
		line = line.strip('\n')
		if line.startswith('\\id'):
			ref = 0
			filepath = line[4:].replace('.cha','.txt')
			filename = re.search(r'.*\/(.*).cha:',line).group(1)
			print(filepath+'\\id '+filename)
		elif line.startswith('\\tx'):
			print(filepath+'\\ref %03d' %ref)
			print(filepath+line)
			ref += 1
		elif line.startswith('\\ELANBegin') or line.startswith('\\ELANEnd'):
			timestamp = float(line.split(' ')[1])
			print(filepath+line.split(' ')[0]+' '+str(timestamp/1000))
		elif line.startswith('\\ft') or line.startswith ('\\cm') or not line:
			print(filepath+line)
		else:
			toprint(= [])
			code = line.split(' ')[0]
			tokens = line.split(' ')[1:]
			print(filepath+code,)
			for token in tokens:
				toprint.append(token + ' '*(19-len(token)))
			print(''.join(toprint))

if __name__=="__main__":
	main()
