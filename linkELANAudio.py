#!/usr/bin/env python
"""
Links audio/video to ELAN file. Update lines 31/32/34 as necessary with the file path.
Use the option -v if there is both video and audio, otherwise it defaults to audio.
"""
import os
import sys
import codecs
from argparse import ArgumentParser,RawTextHelpFormatter

sys.stdout=codecs.getwriter('utf8')(sys.stdout)
sys.stderr=codecs.getwriter('utf8')(sys.stdout)


def main():
	parser = ArgumentParser(description = __doc__, formatter_class=RawTextHelpFormatter)
	parser.add_argument('inputdir', help = 'input')
	parser.add_argument('-v', '--videoandaudio', action='store_true',help='Link audio and video files')
	opts = parser.parse_args()
	for root, dirs, files in os.walk(opts.inputdir, topdown=False):
		for name in files:
			fullpath = os.path.join(root,name)
			if name.endswith(".eaf"):
				newfile = fullpath + '.tmp'
				sys.stdout = open(newfile,'w')
				for line in codecs.open(fullpath):
					line = line.strip('\n')
					session=name.replace('.eaf','')
					if '<HEADER MEDIA_FILE="" TIME_UNITS="milliseconds">' in line:
						print(line)
						if opts.videoandaudio:
							print('        <MEDIA_DESCRIPTOR MEDIA_URL="file:///Volumes/Bob/Corpus_Final/ACLA/'+session+'/'+session+'.mp4" MIME_TYPE="video/mp4" RELATIVE_MEDIA_URL="./'+session+'.mp4"/>')
							print('        <MEDIA_DESCRIPTOR EXTRACTED_FROM="file:///Volumes/Bob/Corpus_Final/ACLA/'+session+'/'+session+'.mp4" MEDIA_URL="file:///Volumes/Bob/Corpus_Final/ACLA/'+session+'/'+session+'.WAV" MIME_TYPE="audio/x-wav" RELATIVE_MEDIA_URL="./'+session+'.WAV"/>')
						else:
							print('        <MEDIA_DESCRIPTOR MEDIA_URL="file:///Volumes/Bob/Corpus_Final/ACLA/'+session+'/'+session+'.mp4" MIME_TYPE="audio/x-wav" RELATIVE_MEDIA_URL="./'+session+'.wav"/>')
					elif not '<MEDIA_DESCRIPTOR' in line:
						print(line)

if __name__=="__main__":
	main()
