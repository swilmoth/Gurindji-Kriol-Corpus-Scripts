#!/usr/bin/env python
"""
This script finds unknown spellings in the transcription tier of a CHAT file.\r
For each .cha file in a given directory (and all subdirectories), tokens on the\r
transcription tier are compared to the lexicon(s) and an optional checked words\r
file.
\r
The output, a 2-column file, can be used to create or add to a checked words\r
file, in conjunction with correctCHATSpelling.py.

"""

import sys
import codecs
from argparse import ArgumentParser,RawTextHelpFormatter
import os
import re

sys.stdout=codecs.getwriter('utf8')(sys.stdout)

def main():
	parser = ArgumentParser(description = __doc__,formatter_class=RawTextHelpFormatter)
	parser.add_argument('-l', '--lexicon', nargs = '+', help = 'lexicon(s)') # Lexicon(s)
	parser.add_argument('-c', '--checkedwords', help = 'checked words') # Checked words (optional)
	parser.add_argument('-d', '--indir',nargs = '+', help = 'directory with .cha files') # Directory with .cha files
	opts = parser.parse_args()

	lexicon = []
	checkedwords = {}
	transcribedwords = []
	lexword = ""
	checkedword = ""

	# read lexicon(s), make list of words
	for infile in opts.lexicon:
		for line in codecs.open(infile, 'r', 'utf8'):
			line = line.strip('\n')
			lexword = line.split(' ')[0]
			lexicon.append(lexword)
	
	# if checkedwords: read checked words file, make list with first column
	if opts.checkedwords:
		for line in codecs.open(opts.checkedwords, 'r', 'utf8'):
				line = line.strip('\n')
				checkedword = line.split('\t')[0]
				correctedword = line.split('\t')[1]
				checkedwords[checkedword] = correctedword

	# go through directory, read transcription lines, make list of transcribed words
	for directory in opts.indir:
		for root, dirs, files in os.walk(directory, topdown=False):
		   for name in files:
		 	  txline = False
		 	  fullpath = os.path.join(root,name)
		 	  if name.endswith(".cha"):
		 		  for line in codecs.open(fullpath, 'r', 'utf8'):
		 			  line = line.strip('\n')
		 			  transcription = []
		 			  if line.startswith('*'):
		 				  txline = True
		 			  elif line.startswith('%') or line.startswith('@'):
		 				  txline = False
		 			  if txline:
		 				  if '\t' in line:
		 					  transcription = line.split('\t')[1]
		 					  for word in transcription.split(' '):
		 						  word = word.strip(r'[\[\],.?!]')
								  word = word.strip('+"')
								  word = word.strip('+[')
		 						  if not u'\u0015' in word:
		 							  if not re.search(r'^[&A-Z]',word) and not re.search(r'^[.,?!]$',word) and word != 'xxx' and word != '+"':
		 								  if word not in transcribedwords:
		 									  transcribedwords.append(word)
	# if it's not in the lexicon or checked words, print
	for word in transcribedwords:
		if opts.checkedwords:
			if word not in checkedwords and word not in lexicon:
				print word+'\t'+word
		else:
			if word not in lexicon:
				print word+'\t'+word

	# if it's not in the lexicon but in the checked words, print in a different section
	checkedwordsmessage = False
	if opts.checkedwords:
		for word in transcribedwords:
			if word not in lexicon and word in checkedwords:
				if not checkedwordsmessage:
					print 
					print '=== The following tokens are already in the \'checked words\' file.==='
					print '===They will be corrected if you run correctCHATSpelling.py. ==='
					print 'Token\tCorrection'
					checkedwordsmessage = True
				print word+'\t'+checkedwords[word]

if __name__=="__main__":
	main()
