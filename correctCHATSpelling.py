#!/usr/bin/env python
"""
This script makes spelling corrections across all CHAT files\r
in a given directory.\r
\r
The inputs are:\r
-c checked words file\r
	This is a two-column (tab-delimited) file with misspellings\r
	in the first column and corrections in the second. This can\r
	include spaces for correcting up to 2 morphemes at once.\r
-d directory with .cha files
	The script will look in all subdirectories of this directory,\r
	correcting any file with a .cha file extension.\r

Any corrections are made in a new file in the same location,\r
with the file extension .cha.corrected
"""

import sys
import codecs
from argparse import ArgumentParser,RawTextHelpFormatter
import os
import re

sys.stdout=codecs.getwriter('utf8')(sys.stdout)

def main():
	parser = ArgumentParser(description = __doc__,formatter_class=RawTextHelpFormatter)
	parser.add_argument('-c', '--checkedwords') # Checked words (optional)
	parser.add_argument('-d', '--indir') # Directory with .cha files
	opts = parser.parse_args()

	# read checked words file, make dictionary
	checkedwords = {}
	for line in codecs.open(opts.checkedwords, 'r', 'utf8'):
		line = line.strip('\n')
		checkedword = line.split('\t')[0]
		correctedword = line.split('\t')[1]
		checkedwords[checkedword] = line.split('\t')[1]

	# go through directory, correct transcriptions
	for root, dirs, files in os.walk(opts.indir, topdown=False):
		for name in files:
			txline = False
			correctedFile = False
			fullpath = os.path.join(root,name)
			if name.endswith(".cha"):
				outfile = []
				for line in codecs.open(fullpath, 'r', 'utf8'):
					line = line.strip('\n')
					transcription = []
					if line.startswith('*'):
						txline = True
					elif line.startswith('%') or line.startswith('@'):
						txline = False
					if txline:
						if '\t' in line:
							transcription = line.split('\t')[1]
							# Formatting fixes
							transcription1 = transcription
							transcription = re.sub(r'([A-Za-z0-9])([.!?])','\\1 \\2',transcription)
							transcription = re.sub(r'\+"([A-Za-z0-9])','+" \\1',transcription)
							transcription = transcription.replace(' ,',',')
							transcription = transcription.replace('  ',' ')
							transcription = transcription.replace(' \t','\t')
							transcription = transcription.replace('\t ','\t')
							transcription = transcription.replace('\t\t','\t')
							transcription = transcription.strip(' ')
							transcription2 = transcription
							if transcription1 != transcription2:
								correctedFile = True
							txlist = transcription.split(' ')
							correcttx = []
							skip = 999 # arbitrary number
							# Spelling corrections
							for index,word in enumerate(txlist):
								# Two-morph corrections
								if index < len(txlist)-1 and word+' '+txlist[index+1] in checkedwords:
									correcttx.append(checkedwords[word+' '+txlist[index+1]])
									skip = index+1
									correctedFile = True
								# Single-morph corrections (skipping over second morph of two-morph corrections)
								elif skip != index:
									if word in checkedwords:
										correcttx.append(checkedwords[word])
										correctedFile = True
									elif word.strip(',') in checkedwords:
										correcttx.append(checkedwords[word.strip(',')] + ',')
										correctedFile = True
									elif word.strip('.') in checkedwords:
										correcttx.append(checkedwords[word.strip('.')] + ' .')
										correctedFile = True
									elif word.strip('!') in checkedwords:
										correcttx.append(checkedwords[word.strip('!')] + ' !')
										correctedFile = True
									elif word.strip('?') in checkedwords:
										correcttx.append(checkedwords[word.strip('?')] + ' ?')
										correctedFile = True
									else:
										correcttx.append(word)
									word = word.rstrip('.?!>')
									
							correctline = line.split('\t')[0] + '\t' + ' '.join(correcttx)
							outfile.append(correctline)
					else:
						outfile.append(line)
				# Printing!
				if correctedFile:
					newFile = fullpath.replace(".cha",".cha.corrected")
					sys.stdout = open(newFile,'w')
					for line in outfile:
						print line.encode('utf8')
					
if __name__=="__main__":
	main()
