#!/usr/bin/env python
"""
Read two directories, including all subdirectories, and make
a new directory with all the old files, except where they
are superseded by files with the same name from the new files.
"""

import os
import re
import sys
import shutil
from argparse import ArgumentParser

def hasNumbers(inputString):
	return bool(re.search(r'\d', inputString))

def main():
	parser = ArgumentParser(usage = __doc__)
	parser.add_argument('mapping', help = 'parent directory of the old files')
	parser.add_argument('old', help = 'parent directory of the old files')
	parser.add_argument('new', help = 'parent directory of the new files')
	args = parser.parse_args()

	olddirs = {} # session (directory) name : fullpath
	newdirs = {} # session (directory) name : fullpath
	stories = {}

	for line in codecs.open(opts.mapping, 'r', 'utf8'):
		line = line.strip('\n')
		session, story = line.split('\t')
		stories[session] = story

	for root, dirs, files in os.walk(args.old,topdown=False):
		dirname = os.path.basename(root)
		if dirname.startswith('F') and hasNumbers(dirname):
			olddirs[dirname] = root
			#print(dirname+'\t'+root)

	for root, dirs, files in os.walk(args.new,topdown=False):
		dirname = os.path.basename(root)
		if dirname.startswith('F') and hasNumbers(dirname):
			if dirname not in olddirs:
				if dirname in stories:
					newdirs[dirname]+'\/'+stories[session] = root
					#print(dirname+'\t'+root)
	
	for session in newdirs:
		for filename in os.listdir(newdirs[session]):
			originpath = os.path.join(newdirs[session], filename)
			targetpath = os.path.join(olddirs[session], filename)
			if os.path.isdir(originpath):
				shutil.copytree(originpath, targetpath)
			else:
				shutil.copy(originpath, targetpath)

	for session in olddirs:
		if session not in newdirs:
			print >> sys.stderr, session+' not found in updated versions'

if __name__=='__main__':
	main()
