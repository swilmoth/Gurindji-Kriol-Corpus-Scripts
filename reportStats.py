#!/usr/bin/env python
"""
Reports various stats on a corpus, by reading all the .cha files
in a particular directory and all subdirectories (unless there's
'original' in the file name).

At the moment, it reports the number of speakers, sessions (.wav
files), utterances, clauses, words, and morphs.

"""

import sys
import codecs
from argparse import ArgumentParser,RawTextHelpFormatter
import os
import re

sys.stdout=codecs.getwriter('utf8')(sys.stdout)

def main():
	parser = ArgumentParser(description = __doc__,formatter_class=RawTextHelpFormatter)
	parser.add_argument('input') # Directory with .cha files
	opts = parser.parse_args()

	speakers = []
	utts = 0
	clauses = 0
	morphs = 0
	words = 0
	filecount = 0
	txtier = False
	mortier = False

	for root, dirs, files in os.walk(opts.input, topdown=False):
		for name in files:
			fullpath = os.path.join(root,name)
			if name.endswith('WAV') or name.endswith('wav') and not 'original' in name:
				filecount += 1
			if name.endswith(".cha") and not 'original' in name:
				txtier = False
				mortier = False
				for line in codecs.open(fullpath, 'r', 'utf8'):
					line = line.strip('\n')

					if line.startswith('%mor'):
						mortier = True
						txtier = False
					elif not line.startswith('\t'):
						mortier = False
						if line.startswith('*'):
							utts += 1
							speaker = line[1:4]
							speakers.append(speaker)
							txtier = True
							mortier = False
						else:
							txtier = False

					if mortier:
						codes = (line.split('\t')[1]).split(' ')
						for code in codes:
							if 'v:tran' in code or 'v:intran' in code  or 'v|' in code:
								clauses += 1
					elif txtier and '\t' in line:
						tx = (line.split('\t')[1]).split(' ')
						for token in tx:
							if token != '.':
								if token != ',':
									if token != '?':
										if token != '+"':
											if token != '!':
												if token != 'xxx':
													if not u'\u0015' in token:
														morphs += 1
														if not token.startswith('_'):
															words += 1
						

	#for speaker in set(speakers):
	#	print(speaker)
	print >> sys.stderr, '%s speakers.' %len(set(speakers))
	print >> sys.stderr, '%s sessions.' %str(filecount)
	print >> sys.stderr, '%s utterances.' %str(utts)
	print >> sys.stderr, '%s clauses.' %str(clauses)
	print >> sys.stderr, '%s words.' %str(words)
	print >> sys.stderr, '%s morphs.' %str(morphs)
					
if __name__=="__main__":
	main()

