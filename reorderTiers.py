#!/usr/bin/env python2.7
"""
This script is used to convert the corpus into an 8-column format
for easy import into OpenRefine. The input file is one large text
file with all the cha files grepped together, including file names
and the beginning of each line. This file also has to have tiers
all on one line (no line-wrapping), gaps inserted between each
utterance, metadata tiers (beginning with @) removed, and a blank
line at the end.
"""

import sys
import re
import codecs
from argparse import ArgumentParser,RawTextHelpFormatter

sys.stdout=codecs.getwriter('utf8')(sys.stdout)
sys.stderr=codecs.getwriter('utf8')(sys.stdout)


def main():
	parser = ArgumentParser(description = __doc__, formatter_class=RawTextHelpFormatter)
	parser.add_argument('input', help = 'input')
	opts = parser.parse_args()
	tx = ''
	start = ''
	prevend = ''
	end = ''
	mor = ''
	eng = ''
	com = []
	for origline in codecs.open(opts.input, 'r', 'utf8'):
		origline = origline.strip('\n')
		origline = re.sub(r'cha:','cha:\t',origline)
		if origline:
			filepath = re.sub(r'cha:\t.*','cha:\t',origline)
			line = re.sub(r'^.*cha:\t','',origline)
			if line.startswith('*'):
				timestamp = re.compile(r'(.*) .([0-9]+)_([0-9]+).$')
				if timestamp.search(line):
					tx = timestamp.search(line).group(1)
					start = timestamp.search(line).group(2)
					end = timestamp.search(line).group(3)
				else:
					tx = line
					prevend = start
			elif line.startswith('%%mor'):
				mor = line[6:]
			elif line.startswith('%%eng'):
				eng = line[6:]
			elif line.startswith('%%com'):
				if not com:
					com = [line[6:]]
				else:
					com.append(line[6:])
			else:
				print origline
		else:
			if tx:
				print filepath,
				print tx+'\t',
				if start:
					print start+'\t',
				else:
					print prevend+'\t',
				if end:
					print end+'\t',
				else:
					print '\t',
				if mor:
					print mor+'\t',
				else:
					print '\t',
				if eng:
					print eng+'\t',
				else:
					print '\t',
				if com:
					print ' '.join(com)
				else:
					print 

			tx = ''
			mor = ''
			eng = ''
			com = []
			start = ''
			end = ''


if __name__=="__main__":
	main()
